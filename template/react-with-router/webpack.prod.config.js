const wc = require('webpack-config')
const path = require('path')
const ManifestPlugin = require('webpack-manifest-plugin')
const serverConfig = require(`./config/${process.env.SERVER_ENV}.config`)

module.exports = new wc.Config().merge({
  mode: 'production',
  output: {
    filename: '[name].[hash].js',
    chunkFilename: '[name].[hash].js',
    path: path.resolve('dist', serverConfig.outputPath),
    publicPath: serverConfig.publicPath,
  },
  plugins: [
    new ManifestPlugin({
      filter: ({ isInitial }) => isInitial,
      sort: (a, b) => {
        const sortList = []
        const sort1 = sortList.indexOf(a.name)
        const sort2 = sortList.indexOf(b.name)

        if (sort1 < sort2) {
          return -1
        } else if (sort1 > sort2) {
          return 1
        }

        return 0
      },
      generate: (seed, files) => {
        const scripts = []
        const styles = []

        files.forEach(({ name, path }) => {
          if (/\.js$/.test(name)) {
            scripts.push(path)
          } else if (/\.css$/.test(name)) {
            styles.push(path)
          }
        })

        return { styles, scripts }
      },
    }),
  ],
})
