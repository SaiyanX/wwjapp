import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter } from 'react-router-dom'
import RouterView from './router'

ReactDOM.render(
  <BrowserRouter>
    <RouterView />
  </BrowserRouter>,
  document.getElementById('react-container')
)
