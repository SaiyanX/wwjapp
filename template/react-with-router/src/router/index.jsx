import React from 'react'
import { Route, Switch } from 'react-router-dom'
import Home from '../page/home/Home'

const RouterView = () => {
  return (
    <Switch>
      <Route exact path="/" render={Home} />
    </Switch>
  )
}

export default RouterView
