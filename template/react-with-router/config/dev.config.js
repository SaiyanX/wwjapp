const baseConfig = require('./base.config')

module.exports = baseConfig({
  publicPath: 'https://template.com/assets/',
})
