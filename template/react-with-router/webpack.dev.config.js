const wc = require('webpack-config')
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = new wc.Config().merge({
  mode: 'development',
  devServer: {
    quiet: true,
    stats: 'minimal',
    open: true,
    port: '4396',
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './src/index.html',
    }),
  ],
})
