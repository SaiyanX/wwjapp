const webpack = require('webpack')
const wc = require('webpack-config')

module.exports = new wc.Config()
  .extend(`webpack.${process.env.NODE_ENV}.config.js`)
  .merge({
    entry: {
      main: './src/index',
    },
    plugins: [
      new webpack.DefinePlugin({
        'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
        'process.env.SERVER_ENV': JSON.stringify(process.env.SERVER_ENV),
      }),
    ],
    resolve: {
      extensions: ['.js', '.jsx'],
    },
    module: {
      rules: [
        {
          test: /\.(js|jsx)$/,
          exclude: /node_modules/,
          use: [
            {
              loader: 'babel-loader',
            },
          ],
        },
      ],
    },
  })
