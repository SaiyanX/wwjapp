import React from 'react'
import ReactDOM from 'react-dom'
import Home from './page/home/Home'

ReactDOM.render(<Home />, document.getElementById('react-container'))
