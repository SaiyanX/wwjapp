module.exports = props => ({
  // 线上资源地址
  publicPath: 'https://template.com/assets/',
  // 打包后dist里的目录，一般与域名后目录相同
  outputPath: 'assets',
  // 压缩包地址
  tarPath: './dist/template.tar.gz',
  ...props,
})
