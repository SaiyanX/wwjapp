// 启动默认 **开发环境**
process.env.NODE_ENV = 'dev'
process.env.SERVER_ENV = process.argv[2] || 'dev' // 项目所处的服务器环境（开发，测试，预发或生产）

const { spawn } = require('child_process')

spawn('webpack-dev-server', ['--progress', '--compress'], { stdio: 'inherit' })
