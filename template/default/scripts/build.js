// 默认 **生产环境**
process.env.NODE_ENV = 'prod' // 项目本身的编译环境（开发或者生产）
process.env.SERVER_ENV = process.argv[2] || 'prod' // 项目所处的服务器环境（开发，测试，预发或生产）

const rimraf = require('rimraf')
const path = require('path')
const { execSync } = require('child_process')
const serverConfig = require(`../config/${process.env.SERVER_ENV}.config`)

// 删除旧代码
rimraf.sync('./dist')

// 生成代码
execSync('webpack', { stdio: 'inherit' })

// 打包代码
execSync(
  `tar -czvf ${serverConfig.tarPath} -C ./dist ${serverConfig.outputPath}`,
  {
    stdio: 'inherit',
  }
)
