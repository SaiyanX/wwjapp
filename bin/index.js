#!/usr/bin/env node

const program = require("commander");

program
  .command("create <app-name>")
  .description("create a new project powered by saiyan")
  .option("-d, --default", "Skip prompts and use default preset")
  .action((name, cmd) => require("../lib/create")(name, cmd));

program.parse(process.argv);
