module.exports = () => {
  const defautPackages = [
    { name: "webpack", dev: true },
    { name: "webpack-cli", dev: true },
    { name: "webpack-config", dev: true },
    { name: "webpack-dev-server", dev: true },
    { name: "html-webpack-plugin", dev: true },
    { name: "react", dev: false },
    { name: "react-dom", dev: false },
    { name: "@babel/runtime", dev: false },
    { name: "@babel/core", dev: true },
    { name: "@babel/plugin-proposal-class-properties", dev: true },
    { name: "@babel/plugin-syntax-dynamic-import", dev: true },
    { name: "@babel/plugin-transform-runtime", dev: true },
    { name: "@babel/preset-env", dev: true },
    { name: "@babel/preset-react", dev: true },
    { name: "babel-loader", dev: true },
    { name: "husky", dev: true },
    { name: "jest", dev: true },
    { name: "prettier", dev: true },
    { name: "pretty-quick", dev: true },
    { name: "rimraf", dev: false },
    { name: "conventional-changelog-cli", dev: true }
  ];

  return defautPackages;
};
