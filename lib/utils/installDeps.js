const execa = require("execa");

module.exports = (command = "npm", targetDir, packages = []) => {
  function executeCommand(command, args, targetDir) {
    return new Promise((resolve, reject) => {
      const child = execa(command, args, {
        cwd: targetDir,
        stdio: ["inherit", "inherit", "inherit"]
      });

      child.on("close", code => {
        if (code !== 0) {
          reject(`command failed: ${command} ${args.join(" ")}`);
          return;
        }

        resolve();
      });
    });
  }

  function installDep() {
    return new Promise(async resolve => {
      const args = [];
      args.push("install", "--loglevel", "error");
      packages.forEach((package, index) => {
        if (!package.dev) {
          args.push(package.name);
        }
      });
      await executeCommand(command, args, targetDir);
      resolve();
    });
  }

  function installDevDep() {
    return new Promise(async resolve => {
      const args = [];
      args.push("install", "--loglevel", "error");
      args.push("-D");
      packages.forEach((package, index) => {
        if (package.dev) {
          args.push(package.name);
        }
      });
      await executeCommand(command, args, targetDir);
      resolve();
    });
  }

  return new Promise(async resolve => {
    await installDep();
    await installDevDep();
    resolve();
  });
};
