const inquirer = require("inquirer");

module.exports = () => {
  return new Promise(resolve => {
    let promps = [];

    promps.push({
      type: "input",
      name: "name",
      message: "请输入项目名称",
      validate: input => {
        if (!input) {
          return "不能为空";
        }
        return true;
      }
    });

    promps.push({
      type: "input",
      name: "description",
      message: "请输入项目描述",
      validate: input => {
        if (!input) {
          return "不能为空";
        }
        return true;
      }
    });

    promps.push({
      type: "checkbox",
      name: "packages",
      message: "选择你需要的模块",
      choices: [
        {
          name:
            "sass - Sass is the most mature, stable, and powerful professional grade CSS extension language in the world.",
          value: "sass",
          short: "sass"
        },
        {
          name:
            "typescript - TypeScript is a superset of JavaScript that compiles to clean JavaScript output.",
          value: "typescript",
          short: "sass"
        },
        {
          name: "react-router-dom - Declarative routing for React.",
          value: "react-router-dom",
          short: "react-router-dom"
        },
        {
          name: "file-loader - Handle your image link in js.",
          value: "file-loader",
          short: "file-loader"
        },
        {
          name:
            "axios - Promise based HTTP client for the browser and node.js.",
          value: "axios",
          short: "axios"
        }
      ]
    });

    inquirer.prompt(promps).then(function(answers) {
      let result = answers;

      result.packages = result.packages.map(package => {
        return {
          name: package,
          dev: package !== "react-router-dom"
        };
      });

      resolve(result);
    });
  });
};
