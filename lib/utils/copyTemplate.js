const fs = require("fs-extra");
const path = require("path");

module.exports = (choices, targetDir) => {
  let tempDir = "";
  if (choices.packages.find(package => package.name === "react-router-dom")) {
    tempDir = "react-with-router";
  } else {
    tempDir = "default";
  }
  return fs.copy(path.resolve(__dirname, "../../template", tempDir), targetDir);
};
