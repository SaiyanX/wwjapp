const fs = require("fs-extra");
const chalk = require("chalk");
const promps = require("./utils/promps");
const writeFileTree = require("./utils/writeFileTree");
const installDeps = require("./utils/installDeps");
const defaultPackages = require("./utils/defaultPackages");
const copyTemplate = require("./utils/copyTemplate");

async function action(projectName, options) {
  const targetDir = projectName;
  if (fs.existsSync(targetDir)) {
    console.log(
      chalk.red(
        `Target directory ${chalk.cyan(
          targetDir
        )} already exists. Change the name and try again.`
      )
    );
    process.exit(1);
  }

  const choices = await promps();

  const packageJson = {
    name: choices.name,
    version: "0.1.1",
    description: "",
    main: "webpack.config.js",
    scripts: {
      test: "node ./scripts/test.js",
      start: "node ./scripts/start.js",
      build: "node ./scripts/build.js",
      prettier:
        'prettier --config "prettier.config.js" --write "./src/**/*.js"',
      version:
        "conventional-changelog -p angular -i CHANGELOG.md -s -r 0 && git add CHANGELOG.md"
    },
    author: "",
    license: "ISC",
    repository: {
      type: "git",
      url: ""
    },
    husky: {
      hooks: {
        "pre-commit": "pretty-quick --staged"
      }
    }
  };

  writeFileTree(targetDir, {
    "package.json": JSON.stringify(packageJson, null, 2)
  });

  const packages = choices.packages.concat(defaultPackages());

  await installDeps("npm", targetDir, packages);

  console.log(chalk.yellow("Successfully installed Packages via npm!"));

  await copyTemplate(choices, targetDir);

  console.log(`Successfully created project ${chalk.yellow(choices.name)}`);
}

module.exports = (...args) => {
  action(...args);
};
